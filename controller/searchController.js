
let deferred = require('../lib/common-utils/deferred');
let moment = require('moment-timezone');
let fs = require('fs');
let data =[];
let spliters = ["product/productId",
"review/userId",
"review/profileName",
"review/helpfulness",
"review/score",
"review/time",
"review/summary",
"review/text"]
let assigners = ["productId",
"userId",
"profileName",
"helpfulness",
"score",
"time",
"summary",
"text"]
//loading file data
let content = fs.readFileSync('foods.txt', "utf8");
let contentSplitted = content.split("\n\n");
for(let i = 0 ; i<contentSplitted.length ; i++){
    let currentContentSplit =  contentSplitted[i].split("\n");
    let tempData ={};
    for(let j = 0 ; j<currentContentSplit.length ; j++){
        let tempSplit = currentContentSplit[j].split(": ");
        tempData[assigners[spliters.indexOf(tempSplit[0])]] = tempSplit[1];
    }
    data.push(tempData);
}
console.log("*************** Data Loaded *************");
console.log("total review :" , data.length);
console.log("sample review :" , data[0]);

//generating load test file
let loadData = data.reduce(function(a , p){
    let totalRandoms = Math.floor(Math.random() * 10) + 1;
    let summery = p.summary.split(" ");
    let text = p.text.split(" ");
    let tempAppend ="";
    while(totalRandoms--){
        let toChooseRand = Math.floor(Math.random() * 3) + 1;
        if(toChooseRand === 1){
            let randPos = Math.floor(Math.random() * (text.length-1)) + 1;
            tempAppend += (text[randPos]+",");
        }
        else{
            let randPos = Math.floor(Math.random() * (summery.length-1)) + 1;
            tempAppend += (summery[randPos]+",");
        }
    }
    a = (a+ tempAppend +"\n")
    return a;
} , "");
loadData=(loadData+loadData+loadData+loadData);
fs.writeFile("loadtest.txt", loadData, function(error){
    if(error)console.log(error);
    else{
        console.log("******** LOAD FILE GENERATED ***********");
    }
});


class SearchAPI {
    search(params) {
        console.log(params.post);
        if(!params.post || !params.post.query || !params.post.query.length){
            return deferred.failure({success:false, error:"query string not present or empty."});
        }
        let limit = 20;
        if(params.post.limit){
            limit = parseInt(params.post.limit);
        }
        let queries=params.post.query;
        if(queries.constructor !== Array){
            return deferred.failure({success:false, error:"send queries inside array."});
        }
        let totalQueries = queries.length;
        console.log("query length" , totalQueries);
        let matchArray = []; //sample obj {matchCount:1 , dataIndex:3}
        let nonZeroScore = Math.round(90/(totalQueries*1.0)) / 100 ;
        let outArr=[];
        data.forEach(function(val , index){
            let tempMatch = {dataIndex:index, matchCount:0};
            let isAnyMatched = false; //reducing data size
                queries.forEach(function(q){
                    assigners.every(function(key){
                        if(val[key].indexOf(q) !== -1){
                            // isAnyMatched = true; 
                            tempMatch.matchCount++;
                            return false;
                        }
                        return true;
                })
            });
            if(isAnyMatched){
                matchArray.push(tempMatch);
                isAnyMatched = false;
            }
        });
        let sortedMatch = matchArray.sort(function(a, b){
             if (a.matchCount < b.matchCount) {
                return 1;
              }
              if (a.matchCount > b.matchCount) {
                return -1;
              }
              return 0;
        });
        if(sortedMatch.length>limit){
            sortedMatch.length = limit;
        }
        else{
            let remains = limit - sortedMatch.length;
            let matchedIds = sortedMatch.map(function(val){
                return val.dataIndex;
            })
            let ind=0;
            while(remains>0){
                if(matchedIds.indexOf(ind) === -1){
                    let tempData = {matchCount:0 , dataIndex:ind};
                    sortedMatch.push(tempData);
                    remains--;
                }
                ind++;
            }
        }
        
        // return deferred.success(sortedMatch);
        outArr = sortedMatch.map(function(val){
            var tempData = JSON.parse(JSON.stringify(data[val.dataIndex]));
            tempData.matchCount = val.matchCount;
            tempData.searchScore = Math.round(((val.matchCount*1.0)/(totalQueries*1.0))*100) / 100;
            if(!val.matchCount){
                tempData.searchScore = nonZeroScore;
            }
            return tempData;
        })
        return deferred.success(outArr);
    }



}

module.exports = SearchAPI;