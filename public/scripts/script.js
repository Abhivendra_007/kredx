$(document).ready(function () {
	$("#search").click(function(){
		var limit = $("#limit").val();
		var url = "http://localhost:10001/search";
		var queryData = $("#search-box").val();
		var arr = queryData.split(",");
		if(arr.length){
			data = {
			  query: arr
			}
			if(limit){
				data.limit = limit;
			}
			console.log("data" , data);
			$.ajax({
			  type: "POST",
			  url: url,
			  data: JSON.stringify(data),
			  headers:{
			  	"Content-Type":"application/json"
			  },
			  success: function(data){
			  	$("#json-text").html("");
			  	data.forEach(function(val){
			  		$("#json-text").append("<br/>"+syntaxHighlight(JSON.stringify(val)));
			  	});
			  	
			  	console.log("Data received");
			  },
			  dataType: "json"
			})
		}
		else{
			alert("empty box");
		}
	});
	function syntaxHighlight(json) {
	    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
	    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
	        var cls = 'number';
	        if (/^"/.test(match)) {
	            if (/:$/.test(match)) {
	                cls = 'key';
	            } else {
	                cls = 'string';
	            }
	        } else if (/true|false/.test(match)) {
	            cls = 'boolean';
	        } else if (/null/.test(match)) {
	            cls = 'null';
	        }
	        return '<span class="' + cls + '">' + match + '</span>';
	    });
	}

	var obj = {a:1, 'b':'foo', c:[false,'false',null, 'null', {d:{e:1.3e5,f:'1.3e5'}}]};
	var str = JSON.stringify(obj, undefined, 4);
});

	