
let express = require('express');
let router = express.Router();

let fn = require('../lib/common-utils/functions');
let SearchAPI = require('../controller/searchController');
let apiObj = new SearchAPI();

function callAPI(req, res, apiMethod) {
    let params = {};
    params = req.params;
    params.headers = req.headers;
    if (req.method.toLowerCase() != 'get') {
        params.post = req.body;
    }

    params.query = req.query;
    params.middlewareStorage = req.middlewareStorage;

    apiMethod(params)
        .success(function (result) {
            res.send(result);
        })
        .failure(function (error) {
            console.logger.error(error);
            if (!(Object.prototype.toString.call(error) === '[object Object]')) {
                error = {success: false, error: error};
            }
            console.logger.error(error);
            res.status(500).send(error);
        });
}

//gets list of vendors
router.post('/', function (req, res) {
    callAPI(req, res, fn.bind(apiObj, 'search'));
});
router.get('/', function(req, res){
  res.render('index' , { path: req.path });
});

module.exports = router;
